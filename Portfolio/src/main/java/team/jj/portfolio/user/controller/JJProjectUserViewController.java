package team.jj.portfolio.user.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import team.jj.portfolio.common.util.JJProjectPathConstants;

@Controller
public class JJProjectUserViewController {
	
	private static final Logger logger = LoggerFactory.getLogger(JJProjectUserViewController.class);
	
	@RequestMapping(value = JJProjectPathConstants.USER_LOGIN_FORM , method = RequestMethod.GET)
	public String userLoginForm(){
		
		logger.info("로그인 폼 이동 시작");
		logger.info("로그인 폼 이동 종료");
		
		return JJProjectPathConstants.PORTFOLIO + JJProjectPathConstants.USER_LOGIN_FORM;
	}
	
	@RequestMapping(value = JJProjectPathConstants.USER_LOGIN_SUCCESS , method = RequestMethod.GET)
	public String login_success(){
		
		logger.info("로그인 성공 시작");
		logger.info("로그인 성공 종료");
		
		return JJProjectPathConstants.PORTFOLIO + JJProjectPathConstants.MAIN;
	}
}
