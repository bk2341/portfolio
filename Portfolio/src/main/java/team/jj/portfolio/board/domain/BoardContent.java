package team.jj.portfolio.board.domain;

public class BoardContent {
	
	/**
	 * 게시판 세부 시퀀스
	 */
	private int board_content_seq;
	
	/**
	 * 게시판 그룹 시퀀스
	 */
	private int board_seq;
	
	/**
	 * 게시판 세부 제목
	 */
	private String board_content_title;
	
	/**
	 * 게시판 세부 내용
	 */
	private String board_content_ct;
	
	/**
	 * 게시판 세부 작성인
	 */
	private String board_content_nm;
	
	/**
	 * 게시판 세부 조회수
	 */
	private int board_content_hit;
	
	/**
	 * 게시판 세부 등록일
	 */
	private String board_content_ins_dt;
	
	/**
	 * 게시판 세부 등록인
	 */
	private String board_content_ins_id;
	
	/**
	 * 게시판 세부 수정일
	 */
	private String board_content_udt_dt;
	
	/**
	 * 게시판 세부 수정인
	 */
	private String board_content_udt_id;
	
	public BoardContent() {
		super();
	}

	public BoardContent(int board_content_seq, int board_seq, String board_content_title, String board_content_ct,
			String board_content_nm, int board_content_hit, String board_content_ins_dt, String board_content_ins_id,
			String board_content_udt_dt, String board_content_udt_id) {
		super();
		this.board_content_seq = board_content_seq;
		this.board_seq = board_seq;
		this.board_content_title = board_content_title;
		this.board_content_ct = board_content_ct;
		this.board_content_nm = board_content_nm;
		this.board_content_hit = board_content_hit;
		this.board_content_ins_dt = board_content_ins_dt;
		this.board_content_ins_id = board_content_ins_id;
		this.board_content_udt_dt = board_content_udt_dt;
		this.board_content_udt_id = board_content_udt_id;
	}

	public int getBoard_content_seq() {
		return board_content_seq;
	}

	public void setBoard_content_seq(int board_content_seq) {
		this.board_content_seq = board_content_seq;
	}

	public int getBoard_seq() {
		return board_seq;
	}

	public void setBoard_seq(int board_seq) {
		this.board_seq = board_seq;
	}

	public String getBoard_content_title() {
		return board_content_title;
	}

	public void setBoard_content_title(String board_content_title) {
		this.board_content_title = board_content_title;
	}

	public String getBoard_content_ct() {
		return board_content_ct;
	}

	public void setBoard_content_ct(String board_content_ct) {
		this.board_content_ct = board_content_ct;
	}

	public String getBoard_content_nm() {
		return board_content_nm;
	}

	public void setBoard_content_nm(String board_content_nm) {
		this.board_content_nm = board_content_nm;
	}

	public int getBoard_content_hit() {
		return board_content_hit;
	}

	public void setBoard_content_hit(int board_content_hit) {
		this.board_content_hit = board_content_hit;
	}

	public String getBoard_content_ins_dt() {
		return board_content_ins_dt;
	}

	public void setBoard_content_ins_dt(String board_content_ins_dt) {
		this.board_content_ins_dt = board_content_ins_dt;
	}

	public String getBoard_content_ins_id() {
		return board_content_ins_id;
	}

	public void setBoard_content_ins_id(String board_content_ins_id) {
		this.board_content_ins_id = board_content_ins_id;
	}

	public String getBoard_content_udt_dt() {
		return board_content_udt_dt;
	}

	public void setBoard_content_udt_dt(String board_content_udt_dt) {
		this.board_content_udt_dt = board_content_udt_dt;
	}

	public String getBoard_content_udt_id() {
		return board_content_udt_id;
	}

	public void setBoard_content_udt_id(String board_content_udt_id) {
		this.board_content_udt_id = board_content_udt_id;
	}

	@Override
	public String toString() {
		return "ArticleContent [board_content_seq=" + board_content_seq + ", board_seq=" + board_seq
				+ ", board_content_title=" + board_content_title + ", board_content_ct=" + board_content_ct
				+ ", board_content_nm=" + board_content_nm + ", board_content_hit=" + board_content_hit
				+ ", board_content_ins_dt=" + board_content_ins_dt + ", board_content_ins_id=" + board_content_ins_id
				+ ", board_content_udt_dt=" + board_content_udt_dt + ", board_content_udt_id=" + board_content_udt_id
				+ "]";
	}
}
