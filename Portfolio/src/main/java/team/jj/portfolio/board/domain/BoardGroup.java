package team.jj.portfolio.board.domain;

public class BoardGroup {
	
	/**
	 * 게시판 그룹 시퀀스
	 */
	private int board_seq;
	
	/**
	 * 게시판 그룹 명
	 */
	private String board_nm;
	
	/**
	 * 게시판 그룹 타입(공통코드)
	 */
	private String board_tp;
	
	/**
	 * 게시판 그룹 구분(공통코드)
	 */
	private String board_gb;
	
	/**
	 * 게시판 그룹 등록인
	 */
	private String board_ins_id;
	
	/**
	 * 게시판 그룹 등록일
	 */
	private String board_ins_dt;
	
	/**
	 * 게시판 그룹 수정인
	 */
	private String board_udt_id;
	
	/**
	 * 게시판 그룹 수정일
	 */
	private String board_udt_dt;
	
	public BoardGroup() {
		super();
	}

	public BoardGroup(int board_seq, String board_nm, String board_tp, String board_gb, String board_ins_id,
			String board_ins_dt, String board_udt_id, String board_udt_dt) {
		super();
		this.board_seq = board_seq;
		this.board_nm = board_nm;
		this.board_tp = board_tp;
		this.board_gb = board_gb;
		this.board_ins_id = board_ins_id;
		this.board_ins_dt = board_ins_dt;
		this.board_udt_id = board_udt_id;
		this.board_udt_dt = board_udt_dt;
	}

	public int getBoard_seq() {
		return board_seq;
	}

	public void setBoard_seq(int board_seq) {
		this.board_seq = board_seq;
	}

	public String getBoard_nm() {
		return board_nm;
	}

	public void setBoard_nm(String board_nm) {
		this.board_nm = board_nm;
	}

	public String getBoard_tp() {
		return board_tp;
	}

	public void setBoard_tp(String board_tp) {
		this.board_tp = board_tp;
	}

	public String getBoard_gb() {
		return board_gb;
	}

	public void setBoard_gb(String board_gb) {
		this.board_gb = board_gb;
	}

	public String getBoard_ins_id() {
		return board_ins_id;
	}

	public void setBoard_ins_id(String board_ins_id) {
		this.board_ins_id = board_ins_id;
	}

	public String getBoard_ins_dt() {
		return board_ins_dt;
	}

	public void setBoard_ins_dt(String board_ins_dt) {
		this.board_ins_dt = board_ins_dt;
	}

	public String getBoard_udt_id() {
		return board_udt_id;
	}

	public void setBoard_udt_id(String board_udt_id) {
		this.board_udt_id = board_udt_id;
	}

	public String getBoard_udt_dt() {
		return board_udt_dt;
	}

	public void setBoard_udt_dt(String board_udt_dt) {
		this.board_udt_dt = board_udt_dt;
	}

	@Override
	public String toString() {
		return "ArticleGroup [board_seq=" + board_seq + ", board_nm=" + board_nm + ", board_tp=" + board_tp
				+ ", board_gb=" + board_gb + ", board_ins_id=" + board_ins_id + ", board_ins_dt=" + board_ins_dt
				+ ", board_udt_id=" + board_udt_id + ", board_udt_dt=" + board_udt_dt + "]";
	}
	
}
