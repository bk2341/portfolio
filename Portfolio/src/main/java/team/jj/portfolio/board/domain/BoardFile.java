package team.jj.portfolio.board.domain;

import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class BoardFile {
	
	/**
	 * 게시판 세부 파일 시퀀스
	 */
	private int board_file_seq;
	
	/**
	 * 게시판 세부 시퀀스
	 */
	private int board_content_seq;
	
	/**
	 * 게시판 세부 파일 저장명
	 */
	private String board_file_saved;
	
	/**
	 * 게시판 세부 파일 원본명
	 */
	private String board_file_origin;
	
	/**
	 * 게시판 세부 파일 구분명(공통코드)
	 */
	private String board_file_gb;
	
	/**
	 * 게시판 세부 파일 등록일
	 */
	private String board_file_ins_dt;
	
	/**
	 * 게시판 세부 파일 등록인
	 */
	private String board_file_ins_id;
	
	/**
	 * 게시판 세부 파일 수정일
	 */
	private String board_file_udt_dt;
	
	/**
	 * 게시판 세부 파일 수정인
	 */
	private String board_file_udt_id;
	
	/**
	 * 게시판 세부 파일
	 */
	private CommonsMultipartFile board_file_upload;
	
	private List<BoardFile> fileList;
	
	public BoardFile() {
		super();
	}

	public BoardFile(int board_file_seq, int board_content_seq, String board_file_saved, String board_file_origin,
			String board_file_gb, String board_file_ins_dt, String board_file_ins_id, String board_file_udt_dt,
			String board_file_udt_id, CommonsMultipartFile board_file_upload) {
		super();
		this.board_file_seq = board_file_seq;
		this.board_content_seq = board_content_seq;
		this.board_file_saved = board_file_saved;
		this.board_file_origin = board_file_origin;
		this.board_file_gb = board_file_gb;
		this.board_file_ins_dt = board_file_ins_dt;
		this.board_file_ins_id = board_file_ins_id;
		this.board_file_udt_dt = board_file_udt_dt;
		this.board_file_udt_id = board_file_udt_id;
		this.board_file_upload = board_file_upload;
	}

	public int getBoard_file_seq() {
		return board_file_seq;
	}

	public void setBoard_file_seq(int board_file_seq) {
		this.board_file_seq = board_file_seq;
	}

	public int getBoard_content_seq() {
		return board_content_seq;
	}

	public void setBoard_content_seq(int board_content_seq) {
		this.board_content_seq = board_content_seq;
	}

	public String getBoard_file_saved() {
		return board_file_saved;
	}

	public void setBoard_file_saved(String board_file_saved) {
		this.board_file_saved = board_file_saved;
	}

	public String getBoard_file_origin() {
		return board_file_origin;
	}

	public void setBoard_file_origin(String board_file_origin) {
		this.board_file_origin = board_file_origin;
	}

	public String getBoard_file_gb() {
		return board_file_gb;
	}

	public void setBoard_file_gb(String board_file_gb) {
		this.board_file_gb = board_file_gb;
	}

	public String getBoard_file_ins_dt() {
		return board_file_ins_dt;
	}

	public void setBoard_file_ins_dt(String board_file_ins_dt) {
		this.board_file_ins_dt = board_file_ins_dt;
	}

	public String getBoard_file_ins_id() {
		return board_file_ins_id;
	}

	public void setBoard_file_ins_id(String board_file_ins_id) {
		this.board_file_ins_id = board_file_ins_id;
	}

	public String getBoard_file_udt_dt() {
		return board_file_udt_dt;
	}

	public void setBoard_file_udt_dt(String board_file_udt_dt) {
		this.board_file_udt_dt = board_file_udt_dt;
	}

	public String getBoard_file_udt_id() {
		return board_file_udt_id;
	}

	public void setBoard_file_udt_id(String board_file_udt_id) {
		this.board_file_udt_id = board_file_udt_id;
	}

	public CommonsMultipartFile getBoard_file_upload() {
		return board_file_upload;
	}

	public void setBoard_file_upload(CommonsMultipartFile board_file_upload) {
		this.board_file_upload = board_file_upload;
	}

	public List<BoardFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<BoardFile> fileList) {
		this.fileList = fileList;
	}

	@Override
	public String toString() {
		return "BoardFile [board_file_seq=" + board_file_seq + ", board_content_seq=" + board_content_seq
				+ ", board_file_saved=" + board_file_saved + ", board_file_origin=" + board_file_origin
				+ ", board_file_gb=" + board_file_gb + ", board_file_ins_dt=" + board_file_ins_dt
				+ ", board_file_ins_id=" + board_file_ins_id + ", board_file_udt_dt=" + board_file_udt_dt
				+ ", board_file_udt_id=" + board_file_udt_id + ", board_file_upload=" + board_file_upload
				+ ", fileList=" + fileList + "]";
	}

}
