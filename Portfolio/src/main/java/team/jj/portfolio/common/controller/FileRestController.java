/**
 * 
 */
package team.jj.portfolio.common.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import team.jj.portfolio.common.util.JJProjectFileService;
import team.jj.portfolio.common.util.JJProjectPathConstants;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : JJProject의 File 관련 Rest Controller
 * 
 */
@RestController
public class FileRestController implements JJProjectPathConstants {
	
	@Value("#{props['temporarilyPath']}")
	private String temporarilyPath;
	
	@ResponseBody
	@RequestMapping(value=JJProjectPathConstants.TEMPORARILY_UPLOAD, method = RequestMethod.POST)
	public HashMap<String, String> temporarilyUpload(MultipartHttpServletRequest request, HttpServletResponse response) {
		HashMap<String, String> result = JJProjectFileService.temporarilySave(request,temporarilyPath);
		return result;
	}
	
}
