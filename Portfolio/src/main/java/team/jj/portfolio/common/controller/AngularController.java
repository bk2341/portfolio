/**
 * 
 */
package team.jj.portfolio.common.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import team.jj.portfolio.common.service.JJProjectCommonService;
import team.jj.portfolio.common.util.JJProjectCodeConverter;
import team.jj.portfolio.common.util.JJProjectPathConstants;
import team.jj.portfolio.exception.GroupIdNotFoundException;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : AngularJS를 위한 샘플 컨트롤러 
 * 
 */
@Controller
public class AngularController implements JJProjectPathConstants {
	
	@Autowired
	private JJProjectCommonService jjcService;
	
	/**
	 * @Method Name : codeconverter
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : 공통코드
	 * @Return : 공통코드명
	 * @Method 설명 : 
	 */
	@ResponseBody
	@RequestMapping(value = "codeconverter", method = RequestMethod.POST,produces = "application/text; charset=utf8")
	public String codeconverter(@RequestParam("code") String code){
		String result = "";
		try {
			result = jjcService.selectCodeName(JJProjectCodeConverter.getCodeMap(code));
		} catch (GroupIdNotFoundException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/sample1", method = RequestMethod.GET)
	public String sample1() {
		return "portfolio/angularSample/sample1";
	}
	
	@RequestMapping(value = "/sample2", method = RequestMethod.GET)
	public String sample2() {
		return "portfolio/angularSample/sample2";
	}
	
	@RequestMapping(value = "/sample3", method = RequestMethod.GET)
	public String sample3() {
		return "portfolio/angularSample/sample3";
	}
	
	@RequestMapping(value = "/sample4", method = RequestMethod.GET)
	public String sample4() {
		return "portfolio/angularSample/sample4";
	}
	
	@RequestMapping(value = "/sample5", method = RequestMethod.GET)
	public String sample5() {
		return "portfolio/angularSample/sample5";
	}
	
	@RequestMapping(value = "/sample6", method = RequestMethod.GET)
	public String sample6() {
		return "portfolio/angularSample/sample6";
	}
	
	@RequestMapping(value = "/sample7", method = RequestMethod.GET)
	public String sample7() {
		return "portfolio/angularSample/sample7";
	}
	
	@RequestMapping(value = "/sample8", method = RequestMethod.GET)
	public String sample8() {
		return "portfolio/angularSample/sample8";
	}
	
	@RequestMapping(value = "/addressTest", method = RequestMethod.GET)
	public String addressTest() {
		return "portfolio/addressSample/addressForm";
	}
	
	
}
