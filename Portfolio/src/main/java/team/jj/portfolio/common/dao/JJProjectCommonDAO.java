package team.jj.portfolio.common.dao;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import team.jj.portfolio.user.domain.User;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : JJProject 공통 기능 DAO
 * 
 */
@Repository
public class JJProjectCommonDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(JJProjectCommonDAO.class);
	
	@Autowired
	SqlSession sqlSession;
	
	/**
	 * @Method Name : selectCodeName
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : GroupId 와 Code 를 가진 HashMap 객체
	 * @Return : 해당 하는 CodeName
	 * @Method 설명 : 서비스로 부터 전달 받은 GroupId와 Code를 Code 이름 검색 매퍼에 전달 하고,
	 * 							매퍼로 부터 전달 받은 Code 이름을 서비스에 전달해준다.
	 */
	public String selectCodeName(HashMap<String, String> fullCode){
		logger.debug("공통 코드 이름 검색 DAO 시작");
		JJProjectCommonMapper mapper = sqlSession.getMapper(JJProjectCommonMapper.class);
		logger.debug("공통 코드 이름 검색 DAO 종료");
		return mapper.selectCodeName(fullCode); 
		
	}
	
	/**
	 * @Method Name : selectUser
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : 회원 아이디
	 * @Return : 회원 정보 객체
	 * @Method 설명 : 전달 받은 회원 아이디의 회원 정보를 조회하는 DAO 함수
	 */
	public User selectUser(User user){
		logger.debug("회원 정보 검색 DAO 시작");
		JJProjectCommonMapper mapper = sqlSession.getMapper(JJProjectCommonMapper.class);
		logger.debug("회원 정보 검색 DAO 종료");
		return mapper.selectUser(user);
	}
	
}
