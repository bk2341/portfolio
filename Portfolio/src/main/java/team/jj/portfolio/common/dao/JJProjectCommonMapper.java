package team.jj.portfolio.common.dao;

import java.util.HashMap;

import team.jj.portfolio.user.domain.User;

/**
 * @Author : 이종호
 * @Date : 2017. 7. 21.
 * @Class 설명 : Soft Engineer Group 공통 기능 매퍼
 * 
 */
public interface JJProjectCommonMapper {
	/**
	 * @Method Name : selectCodeName
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : GroupId 와 Code 를 가진 HashMap 객체
	 * @Return : 해당 하는 CodeName
	 * @Method 설명 : DAO로 부터 전달 받은 GroupId와 Code로 CodeName을 찾은 SQL과 매핑
	 */
	public String selectCodeName(HashMap<String, String> fullCode);
	
	/**
	 * @Method Name : selectUser
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : 회원 아이디
	 * @Return : 회원 정보 객체
	 * @Method 설명 : 전달 받은 아이디로 해당 회원 정보 조회
	 */
	public User selectUser(User user);
}
