/**
 * 
 */
package team.jj.portfolio.common.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : 메일을 보내는 클래스
 * 
 */
/* 
 * TODO: mail.properties에서 불러오지 못하고 있음. 
 * 로딩하는 시점에서 안 되는 것으로 생각. 
 * 로더가 필요할 것으로 보임
 */
@Service
public class JJProjectMailService {
	private Authenticator authenticator;
	
	@Value("#{mail['smtp.user']}")
	private String from;
	
	@Value("#{mail['transport.protocol']}")
	private String protocol;
	
	@Value("#{mail['smtp.host']}")
	private String host;
	
	@Value("#{mail['smtp.port']}")
	private String port;
	
	@Value("#{mail['smtp.starttls.enable']}")
	private String tls;
	
	@Value("#{mail['smtp.socketFactory.class']}")
	private String ssl;
	
	@Value("#{mail['smtp.auth']}")
	private String auth;
	
	@Value("#{mail['admin.username']}")
	private String username;
	
	@Value("#{mail['admin.password']}")
	private String password;
	
	private Properties props_mailing;


	/**
	 * @Method Name : prepareProperties
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : 
	 * @Return : 
	 * @Method 설명 : 메일 보낼 때 사용하는 프로퍼티들을 준비한다
	 */
	private void prepareProperties() {
		if (props_mailing != null) {
			return;
		}
		
		props_mailing = new Properties();
		
		// 프로토콜 설정
		props_mailing.setProperty("mail.transport.protocol", protocol);
		
		// SMTP 서비스 주소(호스트)
		props_mailing.setProperty("mail.smtp.host", host);
		
		// SMTP 서비스 포트 설정
		props_mailing.setProperty("mail.smtp.port", port);
		
		// 로그인 할때 Transport Layer Security(TLS) 설정
        props_mailing.setProperty("mail.smtp.starttls.enable", tls);
        
        // Secure Socket Layer(SSL) 설정
        props_mailing.setProperty("mail.smtp.socketFactory.class", ssl);
        
        // SMTP 인증을 설정
        props_mailing.setProperty("mail.smtp.auth", auth);
        
        // 유저 설정
        /*props_mailing.setProperty("mail.smtp.user", from);*/
	}
	
	/**
	 * @Method Name : prepareAuthenticator
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : 
	 * @Return : 
	 * @Method 설명 : 유저 비밀번호와 아이디를 준비한다
	 */
	private void prepareAuthenticator() {
		if (authenticator != null) {
			return;
		}
		
		authenticator = new Authenticator() {
        	/**
        	 * getPasswordAuthentication
        	 */
        	@Override
        	protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
        	}
		};
	}
	
	/**
	 * @Method Name : sendMail
	 * @Date : 2018. 3. 5.
	 * @User : 이종호
	 * @Param : 
	 * @Return : 
	 * @Method 설명 : 메일을 보낸다
	 */
	public synchronized void sendMail(String fromUser, String toUser, String subject, String text) 
			throws MessagingException {
		// 값을 준비한다
		prepareProperties();
		prepareAuthenticator();
		
		// 메일 세션을 가지고 온다 (무명 클래스 사용)
		Session mailSession = Session.getDefaultInstance(props_mailing, authenticator);

		// 메세지를 작성한다
        Message msg = new MimeMessage(mailSession);
        
        // 메시지를 세팅한다
    	try {
			msg.setFrom(new InternetAddress(fromUser, MimeUtility.encodeText("이종호", "UTF-8", "B")));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toUser, false));
		msg.setSubject(subject);
		//msg.setText(text);
		MimeMultipart multipart = new MimeMultipart("related");
		// html
		BodyPart messageBodyPart = new MimeBodyPart();
		String htmlText = "<img src=\"cid:image\">";
		messageBodyPart.setContent(htmlText, "text/html");
		multipart.addBodyPart(messageBodyPart);
		//image
		messageBodyPart = new MimeBodyPart();
		DataSource fds = new FileDataSource("D:\\article\\profile\\20180403.jpg");
		messageBodyPart.setDataHandler(new DataHandler(fds));
		messageBodyPart.setHeader("Content-ID", "<image>");
		multipart.addBodyPart(messageBodyPart);
		msg.setContent(multipart);
		
		//msg.setContent(text, "text/html");
		msg.setSentDate(new Date());
		
		Transport.send(msg);
	}
	
}
