/**
 * 
 */
package team.jj.portfolio.common.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import team.jj.portfolio.common.util.JJProjectFileService;
import team.jj.portfolio.common.util.JJProjectPathConstants;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : JJProject의 File 관련 View Controller 
 * 
 */
@Controller
public class FIleViewController implements JJProjectPathConstants {
	
	@Value("#{props['temporarilyPath']}")
	private String temporarilyPath;
	
	@RequestMapping(value = JJProjectPathConstants.TEMPORARILY_DOWNLOAD, method = RequestMethod.GET)
	public void temporarilyDownload(HttpServletResponse response, String origin , String saved){
		JJProjectFileService.fileDownload(response, origin, saved,temporarilyPath);
		JJProjectFileService.deleteFile(saved,temporarilyPath);
	}
	
	@RequestMapping(value = JJProjectPathConstants.FILE_DOWNLOAD, method = RequestMethod.GET)
	public void fileDownload(HttpServletResponse response, String origin , String saved, String path){
		JJProjectFileService.fileDownload(response, origin, saved, path);
	}
	
	/*@RequestMapping(value = "/board/imageUpload", method = RequestMethod.POST)
    public String communityImageUpload(MultipartHttpServletRequest request, Model model,String CKEditorFuncNum) {
 
		HashMap<String, String> result = JJProjectFileService.temporarilySave(request,eduApplyCkeditor);
		
		model.addAttribute("file_path", eduApplyCkeditor+"/"+result.get("savedfile"));
		model.addAttribute("CKEditorFuncNum", CKEditorFuncNum);

        return "segroup/society/edu/admin/board_ckeditor";
    }*/
}
