/**
 * 
 */
package team.jj.portfolio.common.util;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : 요청, 응답 경로 중복 확인 인터페이스
 * 
 */
public interface JJProjectPathConstants {

	/**
	 * JJProject 기본 주소
	 */
	public static final String PORTFOLIO = "/portfolio";
	
	/**
	 * 메인 주소
	 */
	public static final String MAIN = "/main";
	
	// Article 관련
	
	/**
	 * Article 기본 주소
	 */
	public static final String ARTICLE = "/article";
	
	/**
	 * Article 호출 주소
	 */
	public static final String ARTICLE_LOAD = ARTICLE + "/article_load";
	
	/**
	 * Article 수정 주소
	 */
	public static final String ARTICLE_UPDATE = ARTICLE + "/article_update";
	
	/**
	 * Article 등록 주소
	 */
	public static final String ARTICLE_INSERT = ARTICLE + "/article_insert";
	
	/**
	 * Article 경력 삭제 주소
	 */
	public static final String ARTICLE_DELETE_WORK = ARTICLE + "/article_delete_work";
	
	/**
	 * Article 메일 전송 주소
	 */
	public static final String ARTICLE_EMAIL_SEND = ARTICLE +"/article_email_send";
	
	// 회원 관련
	
	/**
	 * 회원 기본 주소
	 */
	public static final String USER = "/user";
	
	/**
	 * 회원 로그인 폼 주소
	 */
	public static final String USER_LOGIN_FORM = USER + "/user_login_form";
	
	/**
	 * 회원 로그인 성공 주소
	 */
	public static final String USER_LOGIN_SUCCESS = USER + "/user_login_success";

	// File 관련

	/**
	 * 임시 파일 저장 요청
	 */
	public static final String TEMPORARILY_UPLOAD = "/temporarily_upload";

	/**
	 * 임시 파일 다운로드 요청
	 */
	public static final String TEMPORARILY_DOWNLOAD = "/temporarily_download";

	/**
	 * 파일 다운로드 요청
	 */
	public static final String FILE_DOWNLOAD = "/file_download";

	/**
	 * 파일 업로드 요청
	 */
	public static final String FILE_UPLOAD = "/file_upload";
}
