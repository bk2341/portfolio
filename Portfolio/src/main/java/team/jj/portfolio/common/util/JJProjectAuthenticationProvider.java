/**
 * 
 */
package team.jj.portfolio.common.util;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.AccountExpiredException;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import team.jj.portfolio.common.dao.JJProjectCommonMapper;
import team.jj.portfolio.exception.AccountStatusException;
import team.jj.portfolio.user.domain.User;
import team.jj.portfolio.user.domain.UserSecurity;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : JJProject 인증 클래스
 * 
 */
@Component
public class JJProjectAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	SqlSession sqlSession;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	private static final Logger logger = LoggerFactory.getLogger(JJProjectAuthenticationProvider.class);
	
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		WebApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
		
		sqlSession = (SqlSession) ctx.getBean("sqlSession");
		passwordEncoder = (BCryptPasswordEncoder) ctx.getBean("bcryptPasswordEncoder");
		
		List<GrantedAuthority> roles = new ArrayList<>();
		
		String user_id = (String)authentication.getPrincipal();
		String user_pw	= (String)authentication.getCredentials();
		
		JJProjectCommonMapper mapper = sqlSession.getMapper(JJProjectCommonMapper.class);
		
		User userInfo = new User();
		userInfo.setUser_id(user_id);
		
		User user = mapper.selectUser(userInfo);
		
		//회원 비밀번호 체크
		if(!passwordEncoder.matches(user_pw, user.getUser_pw())){
			logger.debug("사용자 비밀번호가 틀립니다.");
			throw new BadCredentialsException("비밀번호가 틀렸습니다. 다시 입력해 주세요.", new BadCredentialsException("비밀번호 오류"));
		}else{
			if(user.getUser_state().equals("A0201")){
				throw new AccountStatusException("휴면 계정 입니다.", new DisabledException("휴면 계정"));
			}else if(user.getUser_state().equals("A0202")){
				throw new AccountStatusException("탈퇴 계정 입니다.", new AccountExpiredException("탈퇴 계정"));
			}else if(user.getUser_state().equals("A0203")){
				throw new AccountStatusException("정지 계정 입니다.", new LockedException("휴면 계정"));
			}else{
				//회원 권한 부여
				roles.add(new SimpleGrantedAuthority("ROLE_"+user.getUser_flag()));
				UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user_id, user_pw, roles);
				result.setDetails(new UserSecurity(user));
				return result;
			}
		}
	}
}
