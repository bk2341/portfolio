package team.jj.portfolio.article.service;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import team.jj.portfolio.article.dao.JJProjectArticleDAO;
import team.jj.portfolio.board.domain.BoardContent;
import team.jj.portfolio.board.domain.BoardFile;
import team.jj.portfolio.board.domain.BoardGroup;
import team.jj.portfolio.common.util.JJProjectFileService;

@Service
public class JJProjectArticleService {
	
	/**
	 * file.propertices 의 메인 프로필 경로
	 */
	@Value("#{props['article.profile']}")
	private String articleProfilePath;
	
	/**
	 * file.propertices 의 메인 경력 경로
	 */
	@Value("#{props['article.work']}")
	private String articleWorkPath;
	
	/**
	 * file.propertices 의 메인 경력(풀사이즈) 경로
	 */
	@Value("#{props['article.work.fulls']}")
	private String articleWorkFullsPath;
	
	@Autowired
	private JJProjectArticleDAO dao;
	
	private static final Logger logger = LoggerFactory.getLogger(JJProjectArticleService.class);
	
	
	/**
	 * @Method Name : article_load
	 * @Date : 2018. 3. 20.
	 * @User : 이종호
	 * @Param : Article 구분 코드 (공통 코드)
	 * @Return : 해당 코드의 정보
	 * @Method 설명 : 메인 프로필 정보 조회
	 */
	public HashMap<String, Object> article_load(String article_gubun){
		logger.info("메인 프로필 정보 조회 서비스 시작");
		HashMap<String, Object> result = dao.article_load(article_gubun);
		logger.info("메인 프로필 정보 조회 서비스 종료");
		return result;
	}
	
	/**
	 * @Method Name : article_update
	 * @Date : 2018. 3. 20.
	 * @User : 이종호
	 * @Param : Aticle 수정 정보
	 * @Return : -
	 * @Method 설명 : 메인 프로필 정보 수정
	 */
	@Transactional
	public void article_update(BoardGroup boardGroup , BoardContent boardContent , BoardFile boardFile){
		
		logger.info("메인 프로필 정보 수정 서비스 시작");
		//파일이 있는 경우(파일 변경)
		if(!boardFile.getBoard_file_upload().isEmpty()){
			//기존 파일 삭제
			JJProjectFileService.deleteFile(boardFile.getBoard_file_saved(), articleProfilePath);
			//신규 파일 저장
			String articleMainSaved = JJProjectFileService.saveFile(boardFile.getBoard_file_upload(), articleProfilePath);
			boardFile.setBoard_file_saved(articleMainSaved);
			boardFile.setBoard_file_origin(boardFile.getBoard_file_upload().getOriginalFilename());
		}
		
		dao.article_update(boardGroup, boardContent, boardFile);
		
		logger.info("메인 프로필 정보 수정 서비스 종료");
		
	}
	
	/**
	 * @Method Name : article_delete_content
	 * @Date : 2018. 4. 4.
	 * @User : 이종호
	 * @Param : Article Content Sequence
	 * @Return : -
	 * @Method 설명 : 해당 시퀀스의 글을 삭제
	 */
	public void article_delete_content(int board_content_seq, String thumbs_image , String fulls_image){
		logger.info("메인 경력 정보 삭제 서비스 시작");
		
		dao.article_delete_content(board_content_seq);
		
		//기존 파일 삭제
		JJProjectFileService.deleteFile(thumbs_image, articleWorkPath);
		JJProjectFileService.deleteFile(fulls_image, articleWorkFullsPath);
		
		logger.info("메인 경력 정보 삭제 서비스 종료");
	}
	
	/**
	 * @Method Name : article_insert_content
	 * @Date : 2018. 4. 5.
	 * @User : 이종호
	 * @Param : 경력 내용, 경력 파일
	 * @Return : -
	 * @Method 설명 : 해당 경력 정보 등록
	 */
	@Transactional
	public void article_insert_content(BoardContent boardContent , BoardFile boardFile){
		logger.info("메인 경력 정보 등록 서비스 시작");
		
		//경력 정보 파일 체크
		for(BoardFile fileObj : boardFile.getFileList()){
			
			//경력 정보 기존 파일 삭제(수정 시 사용)
			if(fileObj.getBoard_file_saved() != null){
				//파일 구분(A0501 = full , A0502 = thumb)
				if(fileObj.getBoard_file_gb().equals("A0501")){
					JJProjectFileService.deleteFile(fileObj.getBoard_file_saved(), articleWorkFullsPath);
				}else if(fileObj.getBoard_file_gb().equals("A0502")){
					JJProjectFileService.deleteFile(fileObj.getBoard_file_saved(), articleWorkPath);
				}
			}
			
			if(fileObj.getBoard_file_upload() != null && !fileObj.getBoard_file_upload().isEmpty()){
				String fileOrigin = fileObj.getBoard_file_upload().getOriginalFilename();
				
				try{
					String fileSaved = "";
					//파일 구분(A0501 = full , A0502 = thumb)
					if(fileObj.getBoard_file_gb().equals("A0501")){
						fileSaved = JJProjectFileService.saveFile(fileObj.getBoard_file_upload(), articleWorkFullsPath);
					}else if(fileObj.getBoard_file_gb().equals("A0502")){
						fileSaved = JJProjectFileService.saveFile(fileObj.getBoard_file_upload(), articleWorkPath);
					}
					fileObj.setBoard_file_saved(fileSaved);
					fileObj.setBoard_file_origin(fileOrigin);
					
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
		dao.article_insert_content(boardContent, boardFile);
		
		logger.info("메인 경력 정보 등록 서비스 종료");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
