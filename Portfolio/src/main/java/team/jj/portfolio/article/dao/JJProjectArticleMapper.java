package team.jj.portfolio.article.dao;

import java.util.HashMap;

import team.jj.portfolio.board.domain.BoardContent;
import team.jj.portfolio.board.domain.BoardFile;
import team.jj.portfolio.board.domain.BoardGroup;

public interface JJProjectArticleMapper {
	public HashMap<String, Object> article_load(String article_gubun);
	public void article_update_group(BoardGroup boardGroup);
	public void article_update_content(BoardContent boardContent);
	public void article_update_file(BoardFile boardFile);
	public void article_delete_content(int board_content_seq);
	public void article_insert_content(BoardContent boardContent);
	public void article_insert_file(HashMap<String, Object> articleFileMap);
}
