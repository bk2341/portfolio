package team.jj.portfolio.article.controller;

import java.util.HashMap;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import team.jj.portfolio.article.service.JJProjectArticleService;
import team.jj.portfolio.common.util.JJProjectMailService;
import team.jj.portfolio.common.util.JJProjectPathConstants;

@RestController
public class JJProjectArticleRestController {
	
	@Autowired
	private JJProjectArticleService service;
	
	@Autowired
	private JJProjectMailService mail;
	
	/**
	 * @Method Name : article_load
	 * @Date : 2018. 3. 20.
	 * @User : 이종호
	 * @Param : Atricle 구분 코드 (공통코드)
	 * @Return : 해당 코드의 전체 정보
	 * @Method 설명 : 메인 프로필 정보 조회
	 */
	@RequestMapping(value=JJProjectPathConstants.ARTICLE_LOAD, method = RequestMethod.POST)
	public HashMap<String, Object> article_load(String article_gubun){
		HashMap<String, Object> result = service.article_load(article_gubun);
		
		System.out.println(result);
		
		return result;
	}
	
	@RequestMapping(value=JJProjectPathConstants.ARTICLE_EMAIL_SEND, method = RequestMethod.POST)
	public void article_email_send(@RequestBody HashMap<String, String> mailMap){
		System.out.println(mailMap);
		try {
			mail.sendMail(mailMap.get("email"), "amorkimg@naver.com", mailMap.get("subject"), mailMap.get("message"));
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
