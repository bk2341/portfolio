package team.jj.portfolio.article.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import team.jj.portfolio.board.domain.BoardContent;
import team.jj.portfolio.board.domain.BoardFile;
import team.jj.portfolio.board.domain.BoardGroup;

/**
 * @author : 이종호
 * @date : 2018. 3. 22.
 * @desc : 
 * 
 */
@Repository
public class JJProjectArticleDAO {
	
	@Autowired
	private SqlSession sqlsession;
	
	private static final Logger logger = LoggerFactory.getLogger(JJProjectArticleDAO.class);
	
	
	/**
	 * @Method Name : article_load
	 * @Date : 2018. 3. 22.
	 * @User : 이종호
	 * @Param : Article 구분 코드 (공통 코드)
	 * @Return : 해당 코드의 정보
	 * @Method 설명 : 메인 프로필 정보 조회
	 */
	public HashMap<String, Object> article_load(String article_gubun){
		
		logger.info("메인 프로필 정보 조회 DAO 시작");
		JJProjectArticleMapper mapper = sqlsession.getMapper(JJProjectArticleMapper.class);
		HashMap<String, Object> result = null;
		try{
			result = mapper.article_load(article_gubun);
		}catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("메인 프로필 정보 조회 DAO 종료");
		
		return result;
	}
	
	/**
	 * @Method Name : article_update
	 * @Date : 2018. 3. 22.
	 * @User : 이종호
	 * @Param : Aticle 수정 정보
	 * @Return : -
	 * @Method 설명 : 메인 프로필 정보 수정
	 */
	public void article_update(BoardGroup boardGroup , BoardContent boardContent , BoardFile boardFile){
		logger.info("메인 프로필 정보 수정 DAO 시작");
		JJProjectArticleMapper mapper = sqlsession.getMapper(JJProjectArticleMapper.class);
		try{
			mapper.article_update_group(boardGroup);
			boardContent.setBoard_content_seq(boardGroup.getBoard_seq());
			mapper.article_update_content(boardContent);
			//파일이 있는 경우
			if(!boardFile.getBoard_file_upload().isEmpty()){
				boardFile.setBoard_content_seq(boardContent.getBoard_content_seq());
				mapper.article_update_file(boardFile);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		logger.info("메인 프로필 정보 수정 DAO 종료");
	}
	
	
	/**
	 * @Method Name : article_delete_content
	 * @Date : 2018. 4. 4.
	 * @User : 이종호
	 * @Param : Article Content Sequence 
	 * @Return : -
	 * @Method 설명 : 해당 시퀀스의 글을 삭제
	 */
	public void article_delete_content(int board_content_seq){
		logger.info("메인 경력 정보 삭제 DAO 시작");
		
		JJProjectArticleMapper mapper = sqlsession.getMapper(JJProjectArticleMapper.class);
		try{
			mapper.article_delete_content(board_content_seq);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		logger.info("메인 경력 정보 삭제 DAO 종료");
	}
	
	/**
	 * @Method Name : article_insert_content
	 * @Date : 2018. 4. 5.
	 * @User : 이종호
	 * @Param : 경력 내용, 경력 파일
	 * @Return : -
	 * @Method 설명 : 해당 경력 정보 등록
	 */
	@Transactional
	public void article_insert_content(BoardContent boardContent , BoardFile boardFile){
		logger.info("메인 경력 정보 등록 DAO 시작");
		
		JJProjectArticleMapper mapper = sqlsession.getMapper(JJProjectArticleMapper.class);
		
		try {
			mapper.article_insert_content(boardContent);
			
			if(boardFile.getFileList().size() != 0){
				mapper.article_insert_file(makeMap(boardFile.getFileList(), boardContent.getBoard_content_seq(), boardContent.getBoard_content_nm()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.info("메인 경력 정보 등록 DAO 종료");
	}
	
	/**
	 * @Method Name : makeMap
	 * @Date : 2018. 4. 5.
	 * @User : 이종호
	 * @Param : 경력 파일 정보 리스트 , 경력 컨텐트 시퀀트 , 경력 컨텐트 이름
	 * @Return : 입력받은 항목을 가진 맵 객체
	 * @Method 설명 : Mapper에 넘겨줄 맵 객체 생성 함수
	 */
	private <T>HashMap<String, Object> makeMap(List<T> list, int board_content_seq, String board_nm){
		HashMap<String, Object> resultMap = new HashMap<>();
		resultMap.put("board_content_seq", board_content_seq);
		resultMap.put("list", list);
		resultMap.put("board_nm", board_nm);
		return resultMap;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
