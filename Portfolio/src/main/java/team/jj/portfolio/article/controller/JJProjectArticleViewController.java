package team.jj.portfolio.article.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import team.jj.portfolio.article.service.JJProjectArticleService;
import team.jj.portfolio.board.domain.BoardContent;
import team.jj.portfolio.board.domain.BoardFile;
import team.jj.portfolio.board.domain.BoardGroup;
import team.jj.portfolio.common.util.JJProjectPathConstants;

@Controller
public class JJProjectArticleViewController {
	
	@Autowired
	private JJProjectArticleService service;
	
	private static final Logger logger = LoggerFactory.getLogger(JJProjectArticleViewController.class);
	
	/**
	 * @Method Name : article_update
	 * @Date : 2018. 3. 20.
	 * @User : 이종호
	 * @Param : Aticle 수정 정보
	 * @Return : 메인페이지 주소
	 * @Method 설명 : 메인 프로필 정보 수정
	 */
	@RequestMapping(value = JJProjectPathConstants.ARTICLE_UPDATE, method = RequestMethod.POST)
	public String article_update(BoardGroup boardGroup , BoardContent boardContent , BoardFile boardFile){
		
		logger.info("메인 프로필 정보 수정 컨트롤러 시작");
		service.article_update(boardGroup, boardContent, boardFile);
		logger.info("메인 프로필 정보 수정 컨트롤러 종료");
		return "redirect:/";
	}
	
	/**
	 * @Method Name : work_delete
	 * @Date : 2018. 4. 4.
	 * @User : 이종호
	 * @Param : Article Content Sequence
	 * @Return : -
	 * @Method 설명 : 해당 시퀀스의 글을 삭제
	 */
	@RequestMapping(value= JJProjectPathConstants.ARTICLE_DELETE_WORK, method = RequestMethod.POST)
	public String article_delete_work(int board_content_seq, String thumbs_image , String fulls_image){
		
		logger.info("메인 경력 정보 삭제 컨트롤러 시작");
		service.article_delete_content(board_content_seq,thumbs_image,fulls_image);
		logger.info("메인 경력 정보 삭제 컨트롤러 종료");
		return "redirect:/";
	}
	
	/**
	 * @Method Name : article_insert
	 * @Date : 2018. 4. 4.
	 * @User : 이종호
	 * @Param : 경력 내용, 경력 파일
	 * @Return : -
	 * @Method 설명 : 해당 경력 정보 등록
	 */
	@RequestMapping(value=JJProjectPathConstants.ARTICLE_INSERT, method = RequestMethod.POST)
	public String article_insert(BoardContent boardContent , BoardFile boardFile){
		
		logger.info("메인 경력 정보 등록 컨트롤러 시작");
		
		//시큐리티에서 아이디 가져와서 대체해야 함
		boardContent.setBoard_content_nm("admin");
		boardContent.setBoard_content_ins_id("admin");
		boardContent.setBoard_content_udt_id("admin");
		
		service.article_insert_content(boardContent, boardFile);
		
		logger.info("메인 경력 정보 등록 컨트롤러 종료");
		
		return "redirect:/";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
