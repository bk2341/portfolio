package team.jj.portfolio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import team.jj.portfolio.common.util.JJProjectPathConstants;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String root() {
		logger.info("Main 컨트롤러 시작");
		logger.info("Main 컨트롤러 종료");
		//return JJProjectPathConstants.PORTFOLIO+JJProjectPathConstants.MAIN;
		return JJProjectPathConstants.PORTFOLIO+"/index";
	}
	
	@RequestMapping(value = JJProjectPathConstants.MAIN, method = RequestMethod.GET)
	public String main() {
		logger.info("Main 컨트롤러 시작");
		logger.info("Main 컨트롤러 종료");
		return JJProjectPathConstants.PORTFOLIO+JJProjectPathConstants.MAIN;
	}
	
}
