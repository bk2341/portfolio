/**
 * 
 */
package team.jj.portfolio.exception;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : 그룹 코드가 정상적이지 않을 때 발생하는 예외처리 클래스 
 * 
 */
public class GroupIdNotFoundException extends Exception {

	private static final long serialVersionUID = -2774946500907587853L;

	public GroupIdNotFoundException(String message){
		super(message);
	}
}
