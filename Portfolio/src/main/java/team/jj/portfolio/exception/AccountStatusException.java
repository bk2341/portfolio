/**
 * 
 */
package team.jj.portfolio.exception;

/**
 * @Author : 이종호
 * @Date : 2018. 3. 5.
 * @Class 설명 : 
 * 
 */
public class AccountStatusException extends org.springframework.security.authentication.AccountStatusException {
	
	private static final long serialVersionUID = -2014219308794231953L;

	public AccountStatusException(String msg, Throwable t) {
		super(msg, t);
	}

}
