(function($) {

	skel.breakpoints({
		xlarge:		'(max-width: 1680px)',
		large:		'(max-width: 1280px)',
		medium:		'(max-width: 980px)',
		small:		'(max-width: 736px)',
		xsmall:		'(max-width: 480px)',
		xxsmall:	'(max-width: 360px)'
	});

	$(function() {

		var	$window = $(window),
			$body = $('body'),
			$wrapper = $('#wrapper'),
			$header = $('#header'),
			$footer = $('#footer'),
			$main = $('#main'),
			$main_articles = $main.children('article');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			/*$window.on('load', function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
				}, 300);
			});*/
			
			$(function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
					location.hash = '';
				}, 300);
			});

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Fix: Flexbox min-height bug on IE.
			if (skel.vars.IEVersion < 12) {

				var flexboxFixTimeoutId;

				$window.on('resize.flexbox-fix', function() {

					clearTimeout(flexboxFixTimeoutId);

					flexboxFixTimeoutId = setTimeout(function() {

						if ($wrapper.prop('scrollHeight') > $window.height())
							$wrapper.css('height', 'auto');
						else
							$wrapper.css('height', '100vh');

					}, 250);

				}).triggerHandler('resize.flexbox-fix');

			}

		// Nav.
			var $nav = $header.children('nav'),
				$nav_li = $nav.find('li');

			// Add "middle" alignment classes if we're dealing with an even number of items.
				if ($nav_li.length % 2 == 0) {

					$nav.addClass('use-middle');
					$nav_li.eq( ($nav_li.length / 2) ).addClass('is-middle');

				}

		// Main.
			var	delay = 325,
				locked = false;

			// Methods.
				$main._show = function(id, initial) {

					var $article = $main_articles.filter('#' + id);

					// No such article? Bail.
						if ($article.length == 0)
							return;

					// Handle lock.

						// Already locked? Speed through "show" steps w/o delays.
							if (locked || (typeof initial != 'undefined' && initial === true)) {

								// Mark as switching.
									$body.addClass('is-switching');

								// Mark as visible.
									$body.addClass('is-article-visible');

								// Deactivate all articles (just in case one's already active).
									$main_articles.removeClass('active');

								// Hide header, footer.
									$header.hide();
									$footer.hide();

								// Show main, article.
									$main.show();
									$article.show();
									
								// Activate article.
									$article.addClass('active');

								// Unlock.
									locked = false;

								// Unmark as switching.
									setTimeout(function() {
										$body.removeClass('is-switching');
									}, (initial ? 1000 : 0));

								return;

							}

						// Lock.
							locked = true;

					// Article already visible? Just swap articles.
						if ($body.hasClass('is-article-visible')) {

							// Deactivate current article.
								var $currentArticle = $main_articles.filter('.active');

								$currentArticle.removeClass('active');

							// Show article.
								setTimeout(function() {

									// Hide current article.
										$currentArticle.hide();

									// Show article.
										$article.show();

									// Activate article.
										setTimeout(function() {

											$article.addClass('active');

											// Window stuff.
												$window
													.scrollTop(0)
													.triggerHandler('resize.flexbox-fix');

											// Unlock.
												setTimeout(function() {
													locked = false;
												}, delay);

										}, 25);

								}, delay);

						}

					// Otherwise, handle as normal.
						else {

							// Mark as visible.
								$body
									.addClass('is-article-visible');

							// Show article.
								setTimeout(function() {

									// Hide header, footer.
										$header.hide();
										$footer.hide();

									// Show main, article.
										$main.show();
										$article.show();
										load_articles($article);
										
									// Activate article.
										setTimeout(function() {

											$article.addClass('active');

											// Window stuff.
												$window
													.scrollTop(0)
													.triggerHandler('resize.flexbox-fix');

											// Unlock.
												setTimeout(function() {
													locked = false;
												}, delay);

										}, 25);

								}, delay);

						}

				};

				$main._hide = function(addState) {

					var $article = $main_articles.filter('.active');

					// Article not visible? Bail.
						if (!$body.hasClass('is-article-visible'))
							return;

					// Add state?
						if (typeof addState != 'undefined'
						&&	addState === true)
							history.pushState(null, null, '#');

					// Handle lock.

						// Already locked? Speed through "hide" steps w/o delays.
							if (locked) {

								// Mark as switching.
									$body.addClass('is-switching');

								// Deactivate article.
									$article.removeClass('active');

								// Hide article, main.
									$article.hide();
									$main.hide();

								// Show footer, header.
									$footer.show();
									$header.show();

								// Unmark as visible.
									$body.removeClass('is-article-visible');

								// Unlock.
									locked = false;

								// Unmark as switching.
									$body.removeClass('is-switching');

								// Window stuff.
									$window
										.scrollTop(0)
										.triggerHandler('resize.flexbox-fix');

								return;

							}

						// Lock.
							locked = true;

					// Deactivate article.
						$article.removeClass('active');

					// Hide article.
						setTimeout(function() {

							// Hide article, main.
								$article.hide();
								$main.hide();

							// Show footer, header.
								$footer.show();
								$header.show();

							// Unmark as visible.
								setTimeout(function() {

									$body.removeClass('is-article-visible');

									// Window stuff.
										$window
											.scrollTop(0)
											.triggerHandler('resize.flexbox-fix');

									// Unlock.
										setTimeout(function() {
											locked = false;
										}, delay);

								}, 25);

						}, delay);


				};

			// Articles.
				$main_articles.each(function() {

					var $this = $(this);

					// Close.
						$('<div class="close">Close</div>')
							.appendTo($this)
							.on('click', function() {
								location.hash = '';
							});

					// Prevent clicks from inside article from bubbling.
						$this.on('click', function(event) {
							event.stopPropagation();
						});

				});

			// Events.
				$body.on('click', function(event) {

					// Article visible? Hide.
						if ($body.hasClass('is-article-visible'))
							$main._hide(true);

				});

				$window.on('keyup', function(event) {

					switch (event.keyCode) {

						case 27:

							// Article visible? Hide.
								if ($body.hasClass('is-article-visible'))
									$main._hide(true);

							break;

						default:
							break;

					}

				});

				$window.on('hashchange', function(event) {

					// Empty hash?
						if (location.hash == ''
						||	location.hash == '#') {

							// Prevent default.
								event.preventDefault();
								event.stopPropagation();

							// Hide.
								$main._hide();

						}

					// Otherwise, check for a matching article.
						else if ($main_articles.filter(location.hash).length > 0) {

							// Prevent default.
								event.preventDefault();
								event.stopPropagation();

							// Show article.
								$main._show(location.hash.substr(1));

						}

				});

			// Scroll restoration.
			// This prevents the page from scrolling back to the top on a hashchange.
				if ('scrollRestoration' in history)
					history.scrollRestoration = 'manual';
				else {

					var	oldScrollPos = 0,
						scrollPos = 0,
						$htmlbody = $('html,body');

					$window
						.on('scroll', function() {

							oldScrollPos = scrollPos;
							scrollPos = $htmlbody.scrollTop();

						})
						.on('hashchange', function() {
							$window.scrollTop(oldScrollPos);
						});

				}

			// Initialize.

				// Hide main, articles.
					//$main.hide();
					$main_articles.hide();

				// Initial article.
					/*if (location.hash != ''
					&&	location.hash != '#'){
						$(function() {
							// Prevent default.
							event.preventDefault();
							event.stopPropagation();

							// Hide.
							$main._hide();
						});
					}*/
			
					
			// articles ajax
			function load_articles($article){
				var article_gubun = $article.attr("article_gubun");
				$article.find("#update").addClass("display_none");
				$article.find("#show").removeClass("display_none");
				$article.find("#insert").addClass("display_none");
				
				if(!$article.hasClass('load') && !$article.hasClass("mail")){
					$.ajax({
						type : "POST",
						url : "/article/article_load",
						data : {
							'article_gubun' : article_gubun
						},
						dataType : "json",
						error : function(e) {
							alert(JSON.stringify(e));
						},
						success : function(data) {
							
							if(article_gubun == "A0400"){
								//profile 부분
								setProfile($article, data);
							}else if(article_gubun == "A0401"){
								setWork($article, data);
							}
							
						}
					});
				}
			}
			
			//work부분
			function setWork($article, data){
				
				$article.find('#board_seq').val(data.board_seq);
				
				var contentSize = data.articleContent.length;
				var html = '';

				$.each(data.articleContent, function(index, item){
					if(contentSize < 4){
						if(index%2 != 0){
							html += '<div class="12u$ 12u$(xsmall) work-item" >';
						}else{
							html += '<div class="12u 12u$(xsmall) work-item" >';
						}
					}else{
						if(index%2 != 0){
							html += '<div class="6u$ 12u$(xsmall) work-item" >';
						}else{
							html += '<div class="6u 12u$(xsmall) work-item" >';
						}
					}
					var fulls = "";
					var thumbs ="";
					$.each(data.articleFile, function(index2,item2){
						if(item.board_content_seq == item2.board_content_seq){
							if(item2.board_file_gb == "A0501"){
								fulls += '<a href="/file_download?origin='+item2.board_file_origin+'&saved='+item2.board_file_saved+'&path=/article/work/fulls" class="image fit thumb">';
								fulls += '<input type="hidden" class="fulls_image" value="'+item2.board_file_saved+'">';
							}else if(item2.board_file_gb == "A0502"){
								thumbs += '<img src="/file_download?origin='+item2.board_file_origin+'&saved='+item2.board_file_saved+'&path=/article/work" alt="" /></a>';
								thumbs += '<input type="hidden" class="thumbs_image" value="'+item2.board_file_saved+'">';
							}
						}
					});
					html += fulls;
					html += thumbs;
					html +=	'<h3>'+item.board_content_title+'</h3>';
					html +=	'<p>'+item.board_content_ct+'</p>';
					html += '<input type="hidden" class="board_content_seq" value="'+item.board_content_seq+'">';
					html += '<ul class="actions align-center">';
					html += '<li><a class="button special small sbtn work_update_btn">Update</a></li>';
					html += '<li><a class="button special small sbtn work_delete_btn">Delete</a></li></ul></div>';
				});
				//show
				$article.find("#show .row").html(html);
				$article.addClass('load');
				
				//work
				$('#work').poptrox({
					caption: function($a) { return $a.nextAll('h3').text(); },
					overlayColor: '#2c2c2c',
					overlayOpacity: 0.85,
					popupCloserText: '',
					popupLoaderText: '',
					selector: '.work-item a.image',
					usePopupCaption: true,
					usePopupDefaultStyling: false,
					usePopupEasyClose: false,
					usePopupNav: true,
					windowMargin: (skel.breakpoint('small').active ? 0 : 50)
				});
				
				//insert
				$article.find("#work_insert_btn").off("click").on("click",function(){
					$article.find("#insert").removeClass("display_none");
					$article.find("#show").addClass("display_none");
					$(window).scrollTop(0);
				});
				
				//upload
				$article.find("#insert .fa-upload").off("click").on("click",function(){
					$(this).nextAll(".board_file_upload").trigger("click");
				});
				
				//update
				$article.find(".work_update_btn").off("click").on("click",function(){
					
				});
				
				//delete
				$article.find(".work_delete_btn").off("click").on("click",function(){
					
					if(confirm("삭제 하시겠습니까?")){
						var board_content_seq = $(this).closest(".work-item").find(".board_content_seq").val();
						var thumbs_image = $(this).closest(".work-item").find(".thumbs_image").val();
						var fulls_image = $(this).closest(".work-item").find(".fulls_image").val();
					
						$("#hiddenForm").attr("action","/article/article_delete_work");
						$("#board_content_seq").val(board_content_seq);
						$("#thumbs_image").val(thumbs_image);
						$("#fulls_image").val(fulls_image);
						$("#hiddenForm").submit();
					}
					
				});
			}
			
			//profile부분
			function setProfile($article, data){
				//show
				$article.find('#show .major').html(data.articleContent[0].board_content_title);
				$article.find('#show .image img').attr('src','/file_download?origin='+data.articleFile[0].board_file_origin+'&saved='+data.articleFile[0].board_file_saved+'&path=/article/profile');
				$article.find('#show .mb').append(data.articleContent[0].board_content_ct);
				$article.addClass('load');
				
				//update
				$article.find('#board_seq').val(data.board_seq);
				$article.find('#board_content_title').val(data.articleContent[0].board_content_title);
				$article.find('#editorDiv').html(data.articleContent[0].board_content_ct);
				$article.find('#board_file_saved').val(data.articleFile[0].board_file_saved);
				$article.find('.board_file_origin').attr('src','/file_download?origin='+data.articleFile[0].board_file_origin+'&saved='+data.articleFile[0].board_file_saved+'&path=/article/profile');
				$article.find('#update .fa-download').attr('href','/file_download?origin='+data.articleFile[0].board_file_origin+'&saved='+data.articleFile[0].board_file_saved+'&path=/article/profile');
			
				//upload
				$("#update .fa-upload").off("click").on("click",function(){
					if(confirm("기존 이미지가 삭제됩니다. 먼저 다운로드 받으시겠습니까?")){
						$(location).attr('href', '/file_download?origin='+data.articleFile[0].board_file_origin+'&saved='+data.articleFile[0].board_file_saved+'&path=/article/profile');
					}
					$(this).nextAll(".board_file_upload").trigger("click");
				});
				
				//update Btn
				$article.find("#profile_update_btn").off("click").on("click",function(){
					$article.find("#update").removeClass("display_none");
					$article.find("#show").addClass("display_none");
					$(window).scrollTop(0);
				});
			}
			
			
			//이미지 파일 변경시
			$(".board_file_upload").change(function(){
				var val = $(this).val();
				if(val == ""){
				    $(this).val('');
		        	$(this).closest(".field").prevAll(".file-div").find(".board_file_origin").attr("src","/resources/images/noImage.jpg");
		       		return;
				}else if(fileImageCheck(val)){
					photoAjax($(this));
				}else{
					$(this).val('');
					$(this).closest(".field").prevAll(".file-div").find(".board_file_origin").attr("src","/resources/images/noImage.jpg");
		            alert("이미지 파일이 아닙니다. gif, jpg, png 형식만 가능 합니다.");
		            return;
				}
			});
			
			//이미지 업로드 실행함수
			function photoAjax(fileObj){
				
				var size = fileObj[0].files[0].size;
				if(fileSizeCheck(size)){
					var formData = new FormData();
					formData.append("file",fileObj[0].files[0]);
					
					$.ajax({
						type:"POST",						
						url:"/temporarily_upload",				
						data:formData,
						processData: false,
					    contentType: false,
						dataType:"json",				
						success:function(data){	
							fileObj.closest(".field").prevAll(".file-div").find(".board_file_origin").attr('src','/temporarily_download?origin='+data.originalfile+'&saved='+data.savedfile);
						},
						error: function(e){			
							console.log(e);
						}
					});
				}else{
					$(".board_file_upload").val('');
					$(".board_file_upload").closest(".field").prevAll(".file-div").find(".board_file_origin").attr("src","/resources/images/noImage.jpg");
					alert("파일 사이즈는 5MB 까지 입니다.");
				}
			}
			
			//파일 사이즈 체크
			function fileSizeCheck(size){
				//5MB
				var maxSize = 5242880;
				var fileSize = Math.round(size);
				if(fileSize > maxSize){
					return false;
				}
				return true;
			}
			
			//이미지 파일 체크
			function fileImageCheck(name){
				switch(name.substring(name.lastIndexOf('.') + 1).toLowerCase()){
		        case 'gif': case 'jpg': case 'png':
		            return true;
		        default:
		            return false;
		    	}
			}

	});
	
	$("#fontSize").on("change",function(){
		var size = $(this).val();
	    document.execCommand('fontSize', false, size);
	    $("#board_content_ct").val($("#editorDiv").html());
	});
	
	$("#fontName").on("change",function(){
		var size = $(this).val();
	    document.execCommand('fontName', false, size);
	    $("#board_content_ct").val($("#editorDiv").html());
	});
	
	$(".icon").tooltip({
		show: {
			effect : "slideDown",
			delay : 250
		}
	});
	
	$("#sendMailBtn").on("click",function(){
		var name = $("#name").val();
		var email = $("#email").val();
		var subject = $("#subject").val();
		var message = $("#message").val();
		
		//사용자가 입력한 내용을 서버로 전송.
		$.ajax({
			url : "/article/article_email_send",
			type : "post",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify({
				name : name,
				email : email,
				subject : subject,
				message : message
			}),
			success : function(){
				alert("저장되었습니다.");
			},
			error : function(e){
				alert(JSON.stringify(e));
			}
		});
		
	});
	
})(jQuery);

//web editor
function execFn(e){
	var command = $(e).attr("command");
	try{
		if(command == "formatBlock"){
			var text = $(e).text();
			if(text == "Blockquote"){
				var check = $(e).attr("check");
				if(check == "false"){
					document.execCommand(command, false , "blockquote");
					$(e).css("color","#ff6000");
					$(e).attr("check","true");
				}else{
					document.execCommand(command, false , "p");
					$(e).css("color","#ffffff");
					$(e).attr("check","false");
				}
			}else if(text = "Code"){
				var check = $(e).attr("check");
				if(check == "false"){
					document.execCommand(command, false , "dl");
					$(e).css("color","#ff6000");
					$(e).attr("check","true");
				}else{
					$(e).css("color","#ffffff");
					$(e).attr("check","false");
				}
			}
		}else{
			if(command == "bold"){
				var check = $(e).attr("check");
				if(check == "false"){
					$(e).css("color","#ff6000");
					$(e).attr("check","true");
				}else{
					$(e).css("color","#ffffff");
					$(e).attr("check","false");
				}
			}
			document.execCommand(command, false , null);
		}
		$("#board_content_ct").val($("#editorDiv").html());
	}catch(x){
		alert(x.message);
	}
}

//form check
function formCheck(){
	$("#board_content_ct").val($("#editorDiv").html());
	return true;
}










