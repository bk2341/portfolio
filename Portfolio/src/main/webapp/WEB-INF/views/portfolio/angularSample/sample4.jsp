<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html ng-app="myapp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript">
	var myApp = angular.module('myapp', []);
	
	myApp.controller('MainController', ['$scope', function($scope) {
        $scope.users = [
            {"name":"taguchi", "score":52.22},
            {"name":"tanaka", "score":38.22},
            {"name":"yamada", "score":11.11},
            {"name":"test1", "score":22.11},
            {"name":"test2", "score":33.11},
            {"name":"test3", "score":44.11},
            {"name":"test4", "score":55.11},
            {"name":"test5", "score":66.11}
        ];
    }]);
	
	myApp.controller('UserItemController', ['$scope', function($scope){
    	$scope.increment = function() {
    		$scope.user.score++;
    	}
    }]);
	
</script>
<style type="text/css">
	.even {
		background: #ccc;
	}
	.odd {
		background: red;
	}
</style>
</head>
<body ng-controller="MainController">
	<ul>
		<li ng-repeat="user in users" ng-class-even="'even'" ng-class-odd="'odd'">
			{{$index + 1}} {{$first}} {{$middle}} {{$last}} {{user.name|uppercase}}	{{user.score|number:4}}
		</li>
	</ul>
	
	<ul>
		<li ng-repeat="user in users" ng-controller="UserItemController">
			{{user.name}}	{{user.score}}
			<button ng-click="increment()">+1</button>
		</li>
	</ul>
</body>
</html>