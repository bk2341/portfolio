<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript">
	var myApp = angular.module('myapp', []);
	
	myApp.controller('MainController', ['$scope','$http', function($scope,$http) {
		$http({
			method: 'POST',
			url: 'ajax'
		}).then(function successCallback(response) {
			$scope.results = response.data;
		}, function errorCallback(response) {

		});
	}]);
	
	myApp.directive('code', ['$http',function($http){
		return {
			restrict: "E",
			replace: true,
			scope : {
				value : '@'
			},
			controller: function ($scope, $element, $attrs) {
				console.log($attrs.value);
				$http({
					method: 'POST',
					url: 'codeconverter',
			  		responseType: 'text',
					params : {
						code : $attrs.value
					}
				}).then(function successCallback(response) {
		    		$scope.filterParams = response.data;
				}, function errorCallback(response) {
					alert(response);
				});
			}
			,template: "<span>{{filterParams}}</span>"
		}
	}]);
</script>
</head>
<body  ng-app="myapp" ng-controller="MainController">
	<table>
		<tr ng-repeat="res in results">
			<td>{{res.name}}</td>
			<td>{{res.class}}</td>
		</tr>
	</table>
	
	<code value="A0000"></code>
	<code value="A0100"></code>
	<code value="A0202"></code>
</body>
</html>