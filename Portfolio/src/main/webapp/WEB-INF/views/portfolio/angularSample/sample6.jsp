<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html ng-app="myapp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript">
	var myApp = angular.module('myapp', []);
	
	myApp.controller('MainController', ['$scope', function($scope) {
        $scope.users = [
            {"name":"taguchi", "score":52.22},
            {"name":"tanaka", "score":38.22},
            {"name":"yamada", "score":11.11},
            {"name":"test1", "score":22.11},
            {"name":"test2", "score":33.11},
            {"name":"test3", "score":44.11},
            {"name":"test4", "score":55.11},
            {"name":"test5", "score":66.11}
        ];
	
        $scope.addUser = function(){
        	$scope.users.push($scope.user);
        }

	}]);
	
</script>
</head>
<body ng-controller="MainController">
	<form novalidate name="myName" ng-submit="addUser()">
		<p>
			Color:
			<select ng-model="user.color" ng-options=" 'label:'+color for color in ['red','blue','pink']" ng-init="user.color='red'"></select>
		</p>
		<p>
			18+:
			<input type="checkbox" ng-model="user.adult" ng-true-value=" 'adult' " ng-false-value=" 'child' ">
		</p>
		<p>
			Phone:
			<input type="radio" ng-model="user.phone" value="iPhone"> iPhone
			<input type="radio" ng-model="user.phone" value="Android"> Android
		</p>
		<p>
			Memo:
			<textarea ng-model="user.memo" ng-maxlength="140"></textarea>{{140-user.memo.length}}
		</p>
		<p>
			Name:
			<input type="text" name="name" ng-model="user.name" ng-minlength="5" ng-maxlength="8" required>
			<span ng-show="myName.name.$error.required">Required</span>
			<span ng-show="myName.name.$error.minlength">Too short</span>
			<span ng-show="myName.name.$error.maxlength">Too long</span>
		</p>
		<p>
			Score:
			<input type="number" name="score" ng-model="user.score">
		</p>
		<p>
			Email:
			<input type="email" name="email" ng-model="user.email">
			<span ng-show="myName.email.$error.email">Not valid email!!</span>
		</p>
		<p>
			Web:
			<input type="url" name="url" ng-model="user.url">
			<span ng-show="myName.url.$error.url">Not valid url</span>
		</p>
		<p><input type="submit" value="add"></p>
	</form>
	
	<pre>{{user|json}}</pre>
	
	<ul>
		<li ng-repeat="user in users">
			{{user.name}}	{{user.score}}
		</li>
	</ul>
</body>
</html>