<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript">
	var myApp = angular.module('myapp', []);
	
	// 커스텀 지시자 포커스 생성
	myApp.directive('focus', function(){
		return {
			link : function(scope, element, attrs) {
				element[0].focus();
			}
		}
	});
	
	// 커스텀 지시자 hello 태그 생성
	myApp.directive('hello', function(){
		return {
			restrict: "E",
			replace: true,
			template: "<div>Hello readers , thank you for coming</div>"
		}
	});
</script>
</head>
<body  ng-app="myapp">
	name : <input type="text">
	phone : <input type="text" focus>
	
	<hello></hello>
</body>
</html>