<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<article id="contact" class="mail">
	<h2 class="major">Contact</h2>
	<form>
		<div class="field half first">
			<label for="name">Name</label>
			<input type="text" name="name" id="name" />
		</div>
		<div class="field half">
			<label for="email">Email</label>
			<input type="text" name="email" id="email" />
		</div>
		<div class="field">
			<label for="subject">Subject</label>
			<input type="text" name="subject" id="subject" />
		</div>
		<div class="field">
			<label for="message">Message</label>
			<textarea name="message" id="message" rows="4"></textarea>
		</div>
		<ul class="actions">
			<li><input type="button" value="Send Message" class="special" id="sendMailBtn" /></li>
			<li><input type="reset" value="Reset" /></li>
		</ul>
	</form>
	<ul class="icons">
		<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
		<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
		<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
		<li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
	</ul>
</article>