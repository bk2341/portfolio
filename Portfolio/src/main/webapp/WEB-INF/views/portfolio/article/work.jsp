<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<article id="work" article_gubun="A0401">
	<div id="show">
		<h2 class="major">Recent Work</h2>
		<div class="row">
		</div>
		<a class="button special" id="work_insert_btn">Insert</a>
	</div>
	<div id="update">
		test
	</div>
	<div id="insert">
		<form method="post" action="/article/article_insert" enctype="multipart/form-data">
			<input type="hidden" name="board_seq" id="board_seq">
			<h2 class="major">Recent Work</h2>
			<div>
				<div class="field">
					<label for="board_content_title">Title</label>
					<input type="text" name="board_content_title" id="board_content_title" />
				</div>
				<div class="field">
					<label for="board_content_ct">Content</label>
					<textarea name="board_content_ct" id="board_content_ct" placeholder="Enter your message" rows="6"></textarea>
				</div>
			</div>
			<div>
				<div class="field file-div">
					<label for="board_file_saved">Thumb</label>
					<span class="image main half"><img class="board_file_origin" src="<c:url value="/resources/images/noImage.jpg" />" alt="" /></span>
				</div>
				<div class="field half first">
				</div>
				<div class="field align-right">
					<a class="button special icon fa-upload">Up</a>
					<input type="file" name="fileList[0].board_file_upload" class="board_file_upload display_none"  accept="image/*">
					<input type="hidden" name="fileList[0].board_file_gb" value="A0502">
					<a class="button icon fa-download">Down</a>
				</div>
			</div>
			<div>
				<div class="field file-div">
					<label for="board_file_saved">Full</label>
					<span class="image main"><img class="board_file_origin" src="<c:url value="/resources/images/noImage.jpg" />" alt="" /></span>
				</div>
				<div class="field half first">
				</div>
				<div class="field align-right">
					<a class="button special icon fa-upload">Up</a>
					<input type="file" name="fileList[1].board_file_upload" class="board_file_upload display_none"  accept="image/*">
					<input type="hidden" name="fileList[1].board_file_gb" value="A0501">
					<a class="button icon fa-download">Down</a>
				</div>
				<hr />
				<ul class="actions">
					<li><input type="submit" value="Send Update" class="special" /></li>
					<li><input type="reset" value="Reset" /></li>
				</ul>
			</div>
		</form>
	</div>
	<form action="" method="post" class="display_none" id="hiddenForm">
		<input type="hidden" name="board_content_seq" id="board_content_seq">
		<input type="hidden" name="thumbs_image" id="thumbs_image">
		<input type="hidden" name="fulls_image" id="fulls_image">
	</form>
 </article>