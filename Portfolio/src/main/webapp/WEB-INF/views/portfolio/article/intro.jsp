<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<article id="intro" article_gubun="A0400">
	<div id="show">
		<h2 class="major"></h2>
		<span class="image main"><img src="" alt="" /></span>
		<div class="mb"></div>
		<a class="button special" id="profile_update_btn">Update</a>
	</div>
	<div id="update">
		<form method="post" action="/article/article_update" enctype="multipart/form-data" onsubmit="return formCheck();">
			<input type="hidden" name="board_gb" value="A0400">
			<input type="hidden" name="board_seq" id="board_seq">
			<div class="field">
				<label for="board_content_title">Title</label>
				<input type="text" name="board_content_title" id="board_content_title" />
			</div>
			<div class="field">
				<label for="board_content_ct">Contents</label>
				<ul class="icons">
					<li>
						<a href="javascript:;" class="icon fas fa-bold" command="bold" onclick="execFn(this)" check="false" title="Bold"><span class="label">Bold</span></a>
					</li>
					<li class="md">
						<div class="select-wrapper">
							<select id="fontName">
								<option value="고딕">고딕</option>
								<option value="궁서">궁서</option>
								<option value="굴림">굴림</option>
								<option value="돋음" selected="selected">돋음</option>
								<option value="바탕">바탕</option>
								<option value="Serif">Serif</option>
								<option value="Sans-Serif">Sans-Serif</option>
								<option value="Consolas">Consolas</option>
								<option value="Monaco">Monaco</option>
							</select>
						</div>
					</li>
					<li class="sm">
						<div class="select-wrapper">
							<select id="fontSize">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5" selected="selected">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
							</select>
						</div>
					</li>
					<li>
						<a href="javascript:;" class="icon fas fa-align-left" command="justifyLeft" onclick="execFn(this)" title="Left"><span class="label">Left</span></a>
					</li>
					<li>
						<a href="javascript:;" class="icon fas fa-align-center" command="justifyCenter" onclick="execFn(this)" title="Center"><span class="label">Center</span></a>
					</li>
					<li>
						<a href="javascript:;" class="icon fas fa-align-right" command="justifyRight" onclick="execFn(this)" title="Right"><span class="label">Right</span></a>
					</li>
					<li>
						<a href="javascript:;" class="icon fas fa-quote-left" command="formatBlock" onclick="execFn(this)" check="false" title="Blockquote"><span class="label">Blockquote</span></a>
					</li>
					<li>
						<a href="javascript:;" class="icon fas fa-list-ol" command="insertOrderedList" onclick="execFn(this)" title="OrderList"><span class="label">OrderedList</span></a>
					</li>
					<li>
						<a href="javascript:;" class="icon fas fa-list-ul" command="insertUnorderedList" onclick="execFn(this)" title="UnorderList"><span class="label">UnorderedList</span></a>
					</li>
				</ul>
				<ul class="icons">
					<li>
						<a href="javascript:;" class="icon fas fa-code" command="formatBlock" onclick="execFn(this)" check="false" title="Code"><span class="label">Code</span></a>
					</li>
					<li>
						<a href="javascript:;" class="icon fas fa-indent" command="indent" onclick="execFn(this)" title="Indent"><span class="label">Indent</span></a>
					</li>
				</ul>
				<div class="editor" id="editorDiv" contenteditable="true">
				</div>
				<input type="hidden" name="board_content_ct" id="board_content_ct" />
			</div>
			<div class="field file-div">
				<label for="board_file_saved">Image</label>
				<span class="image main"><img class="board_file_origin" src="" alt="" /></span>
			</div>
			<div class="field half first">
			</div>
			<div class="field align-right">
				<a class="button special icon fa-upload">Up</a>
				<input type="hidden" id="board_file_saved" name="board_file_saved">
				<input type="file" name="board_file_upload" class="board_file_upload display_none"  accept="image/*">
				<a class="button icon fa-download">Down</a>
			</div>
			<hr />
			<ul class="actions">
				<li><input type="submit" value="Send Update" class="special" /></li>
				<li><input type="reset" value="Reset" /></li>
			</ul>
		</form>
	</div>
</article>